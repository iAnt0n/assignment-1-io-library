%define SYS_WRITE 1
%define SYS_EXIT 60
%define stdin 0
%define stdout 1

section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi 
    call string_length
    mov rdx, rax
    pop rsi
    mov rdi, stdout
    mov rax, SYS_WRITE
    syscall
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA ; tail call print_char


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, SYS_WRITE
    mov rdi, SYS_WRITE
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi ; no ret because we have a print_uint tail call


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, rsp ; r8 becomes a pointer to memory on stack
    sub rsp, 24 ; allocate 24 bytes (21 bytes to store maximum 8 byte uint as string, and align)
                ; i tried to use r8 as rsp manually and make `mov rsp, r8`, resulting in unaligned rsp before `call print_string`
                ; it works but i guess stack read/writes are slower this way? are there any other consequences?
    dec r8
    mov byte[r8], 0 ; null terminator
    mov rax, rdi
    mov r9, 10
.loop:
    xor rdx, rdx
    div r9
    add dl, '0'
    dec r8
    mov [r8], dl
    test rax, rax
    jnz .loop
    mov rdi, r8
    call print_string
    add rsp, 24
    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
.loop:
    mov r8b, [rdi+rax]
    mov r9b, [rsi+rax]
    inc rax
    cmp r8b, r9b
    je .is_end
    xor rax, rax
    ret
.is_end:
    test r8b, r8b
    jnz .loop
    mov rax, 1
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    push stdin
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор


read_word:
    dec rsi
    push rdi
    push rsi

.skip_spaces:
    call read_char
    cmp al, 0x20
    je .skip_spaces
    cmp al, 0x9
    je .skip_spaces
    cmp al, 0xA
    je .skip_spaces

    pop rsi
    pop rdi
    xor rdx, rdx
.write_char:
    cmp rsi, rdx
    je .ret_fail
    test al, al
    jz .end_word
    mov [rdi+rdx], al
    inc rdx

    push rdi
    push rsi
    push rdx
    call read_char
    pop rdx
    pop rsi
    pop rdi

    cmp rax, 0x20
    je .end_word
    cmp rax, 0x9
    je .end_word
    cmp rax, 0xA
    je .end_word
    jmp .write_char
.end_word:
    mov byte[rdi+rdx], 0
    mov rax, rdi
    ret
.ret_fail:
    xor rax, rax
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r8, r8
    xor r9, r9
    mov r10, 10
.parse_digit:
    mov r9b, [rdi+r8]
    cmp r9b, '9'
    ja .end
    cmp r9b, '0'
    jb .end
    sub r9b, '0'
    inc r8
    mul r10
    add rax, r9
    jmp .parse_digit
.end:
    mov rdx, r8
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor r8, r8
    mov r8b, [rdi]
    cmp r8b, '-'
    je .parse_negative_int
    call parse_uint
    ret
.parse_negative_int:
    inc rdi
    call parse_uint
    test rdx, rdx
    je .end
    inc rdx
    neg rax
.end:    
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
.loop:
    cmp rax, rdx
    je .end_fail
    mov r8b, [rdi+rax]
    mov [rsi+rax], r8b
    cmp r8b, 0
    je .end_ok
    inc rax
    jmp .loop
.end_fail:
    xor rax, rax
.end_ok:
    ret
